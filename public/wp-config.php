<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

//https://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy
// in some setups HTTP_X_FORWARDED_PROTO might contain
// a comma-separated list e.g. http,https
// so check for https existence
if (
	isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
	strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false &&
	!getenv('DEV')
) {
	define('FORCE_SSL_ADMIN', false);
	$_SERVER['HTTPS'] = 'on';
}

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', getenv('DB_NAME'));

/** MySQL database username */
define('DB_USER', getenv('DB_USER'));

/** MySQL database password */
define('DB_PASSWORD', getenv('DB_PASSWORD'));

/** MySQL hostname */
define('DB_HOST', getenv('DB_HOST'));

if (getenv('DB_SOCK')) {
	define('DB_SOCK', getenv('DB_SOCK'));
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0~]q/+QV!L-EwT$K[&@&H3h@4X=+0V| Phk)U=SVw=P|N2M)t!N_&~gu:,OH0ij_');
define('SECURE_AUTH_KEY',  ' gv5! i/MVDDRo1D</[s0Jm-+vb%o}WA.OHAZph0j=nd4WvZ|Oovj*]+.V7 nRk$');
define('LOGGED_IN_KEY',    'y9SGlHqia%M&,5O+tv2v9xPB,TeU4-rksY=hm|[}/Lx12xiS+N~|`]PR F{5>I}c');
define('NONCE_KEY',        ',.(gLj#xC[oGwz5JsN;^B~v7-i{8E?~bN*Q}_DY-+%*@4fU(Yj)G68KXs|/#(V)y');
define('AUTH_SALT',        '`EUE<*iz/}.NIzt<M!UJFe;a{WOzM^$YWUe0E+6+HfaQ75VEv)|!6K|2mTl6&4zs');
define('SECURE_AUTH_SALT', '|_F_dF2`YDz$M^P2=#%B+qs|+f,xmfs.$|3|V&-4y(c`Z]8RY%p:|RZi^nB7UlSG');
define('LOGGED_IN_SALT',   'aop`-+/mXEfLgh}7[q+6a;BJ,*gZ>P3x}!},+GYW*8gXli(RmoY:v,MI_qj-p(4&');
define('NONCE_SALT',       '^>92d:x:w,zpVjV}MUF} GIoB^8;_N7g|!T}_QiL+-k+4NC4;DN77DVkI^F9W7g(');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

@ini_set('log_errors', 'Off');
@ini_set('error_log', ABSPATH . 'wp-content/php-errors.log');

define('WP_DEBUG', false);
// define('DISABLE_WP_CRON', true);
define('DISALLOW_FILE_EDIT', true);
define('DISALLOW_FILE_MODS', true);
define('AUTOMATIC_UPDATER_DISABLED', true);
define('WP_AUTO_UPDATE_CORE', false);
// Just in case
define('WP_MEMORY_LIMIT', '900M');
// Set default theme to API
// define('WP_DEFAULT_THEME', 'api');

// Max revisions
define('WP_POST_REVISIONS', 5);
define('CONCATENATE_SCRIPTS', true); //disables compression and concatenation of scripts and CSS,
define('COMPRESS_SCRIPTS', true); //disables compression of scripts,
define('COMPRESS_CSS', true); //disables compression of CSS,
define('ENFORCE_GZIP', true); //forces gzip for compression (default is deflate).
define('WP_CONTENT_DIR', ABSPATH . 'wp-content');

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
	define('ABSPATH', dirname(__FILE__) . '/');
}

$protocol = 'https://';

if (getenv('DEV')) {
	$protocol = 'http://';
}

define('WP_SITEURL', $protocol . $_SERVER['HTTP_HOST']);
define('WP_HOME', $protocol . $_SERVER['HTTP_HOST']);

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

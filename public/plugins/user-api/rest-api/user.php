<?php
/*
  Managed by AponUserAuthCore
*/
add_action('rest_api_init', function () {
  // Get User
  register_rest_route('v2', '/auth/user', array(
    'methods'  => 'POST',
    'permission_callback' => function () {
      return is_user_logged_in();
    },
    'callback' => function ($x) {
      return AponUserAuthCore::getUser();
    },
  ));
  // Create User
  register_rest_route('v2', '/auth/user/register', array(
    'methods'  => 'post',
    'permission_callback' => function () {
      return !is_user_logged_in();
    },
    'callback' => function ($x) {
      return AponUserAuthCore::createUserByEmail($x);
    },
  ));
  // For uwae
  register_rest_route('v3', '/auth/user/register', array(
    'methods'  => 'post',
    'permission_callback' => function () {
      return !is_user_logged_in();
    },
    'callback' => function ($x) {
      return AponUserAuthCore::createUserByEmailAndPassword($x);
    },
  ));
  // Update User profile
  register_rest_route('v2', '/auth/user', array(
    'methods'  => 'put',
    'permission_callback' => function () {
      return is_user_logged_in();
    },
    'callback' => function ($x) {
      return AponUserAuthCore::update($x);
    },
  ));
  // Send Password Link to Email
  register_rest_route('v2', '/auth/password', array(
    'methods'  => 'post',
    'permission_callback' => function () {
      return !is_user_logged_in();
    },
    'callback' => function (WP_REST_Request $request) {
      $y = $request;
      $origin = $request->get_header('origin');
      return AponUserAuthCore::sendResetPasswordViaEmail($y, $origin);
    },
  ));
  // Change password
  register_rest_route('v2', '/auth/password', array(
    'methods'  => 'put',
    'permission_callback' => function () {
      return !is_user_logged_in();
    },
    'callback' => function ($x) {
      return AponUserAuthCore::changePassword($x);
    },
  ));
  // Check password key
  register_rest_route('v2', '/auth/password-key', array(
    'methods'  => 'put',
    'permission_callback' => function () {
      return !is_user_logged_in();
    },
    'callback' => function ($x) {
      return AponUserAuthCore::checkPasswordKey($x);
    },
  ));
});

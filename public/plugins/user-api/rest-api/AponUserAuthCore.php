<?php
//
class AponUserAuthCore
{
  public static function generateRandomString($length = 12)
  {
    return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
  }
  public static function extractAvatarImage($username, $id)
  {
    // Check if this user has `custom_profile_picture` field
    $customProfilePicture = get_field('custom_profile_picture', "user_$id");
    if (strlen(trim($customProfilePicture)) > 0) {
      return $customProfilePicture;
    }
    if (!$username) return get_avatar_url($id);
    $x = AponUserAuthCore::extractSocial($username);
    if ($x) return 'https://graph.facebook.com/' . $x['id'] . '/picture?type=large&width=360&height=360';
    return get_avatar_url($id);
  }
  public static function extractSocial($username)
  {
    $explodedUsername = explode('_', $username);
    if (count($explodedUsername) > 0 && $explodedUsername[0] === 'facebook') {
      return [
        'channel' => $explodedUsername[0],
        'id' => $explodedUsername[1]
      ];
    }
    return false;
  }
  public static function getUser()
  {
    $user         = null;
    $current_user = wp_get_current_user();
    $id           = $current_user->ID;
    $username     = $current_user->user_login;
    if ($id) {
      $user = [
        '_id'              => $id,
        '_email'           => $current_user->user_email,
        '_first_name'      => $current_user->user_firstname,
        '_last_name'       => $current_user->user_lastname,
        '_role'            => $current_user->roles,
        '_image' => AponUserAuthCore::extractAvatarImage($username, $id),
        '_social' => AponUserAuthCore::extractSocial($username)
      ];
      $acf = get_fields('user_' . $id);
      if ($acf) {
        $user = array_merge($user, (array) $acf);
      }
    }
    $curent_login_time = get_user_meta($id, 'current_login', true);
    //add or update the last login value for logged in user
    if (!empty($curent_login_time)) {
      update_usermeta($id, 'last_login', $curent_login_time);
      update_usermeta($id, 'current_login', current_time('mysql'));
    } else {
      update_usermeta($id, 'current_login', current_time('mysql'));
      update_usermeta($id, 'last_login', current_time('mysql'));
    }
    return $user;
  }

  public static function registerLoginSocial(WP_REST_Request $request)
  {
    $channel = $request['channel'];
    $token = $request['token'];
    $fb = new \Facebook\Facebook([
      'app_id' => getenv('FB_APP_ID'),
      'app_secret' => getenv('FB_APP_SECRET'),
      'default_graph_version' => 'v5.0',
      'default_access_token' => $token, // optional
    ]);
    $response = $fb->get('/me?fields=id,email,name');
    $user = $response->getGraphUser();
    $name = $user['name'];
    $splitName = explode(' ', $name);
    $tmpUsername = $channel . '_' . $user['id'];
    $userId = username_exists($tmpUsername);
    if (!$userId) {
      // If no email allowed
      if (!$user['email']) {
        return [
          'success' => false,
          'error_code' => 'email_not_provided',
          'error_message' => 'Email is not provided by user.'
        ];
      }
      // User not exists, register with $tmpUsername
      $email = email_exists($user['email']);
      if ($email) {
        // Email has already been used.
        return [
          'success' => false,
          'error_code' => 'email_from_social_already_used',
          'error_message' => 'Email has already been used. Please log in using your email instead.'
        ];
      }
      $user_data = array(
        'user_login' => $tmpUsername,
        'user_pass'  => AponUserAuthCore::generateRandomString(),
        'user_email' => $user['email'],
        'first_name' => $splitName[0],
        'last_name'  => count($splitName) > 1 ? $splitName[1] : ''
      );
      $userId = wp_insert_user($user_data);
      update_field('is_verified', true, "user_$userId");
      if (is_wp_error($userId)) {
        return [
          'success' => false,
          'error_code' => $userId,
          'error_message' => $userId
        ];
      }
    }
    $jwt = AAM_Service_Jwt::getInstance()->issueToken($userId);
    return [
      'success' => true,
      'token' => $jwt->token
    ];
  }

  public static function changePassword(WP_REST_Request $request)
  {
    $curr_pass    = $request['currentPassword'];
    $new_pass     = $request['newPassword'];
    // $password = trim($request['password']);
    if (strlen($new_pass) < 8 || strlen($new_pass) > 16) {
      return [
        'success' => false,
        'error_code' => 'password_too_short_or_long',
        'error_message' => 'Password is either too short or too long.'
      ];
    }
    $current_user = wp_get_current_user();
    if ($current_user && wp_check_password($curr_pass, $current_user->data->user_pass, $current_user->ID)) {
      // Valid
      wp_set_password($new_pass, $current_user->ID);
      // Update verified
      update_field('is_verified', true, "user_$current_user->ID");
      return true;
    } else {
      return false;
    }
  }

  public static function checkPasswordKey(WP_REST_Request $request)
  {
    $key   = $request['key'];
    $login = $request['login'];
    $fields = $request['fields'] ?? [];
    $user  = check_password_reset_key($key, $login);
    if (strlen($request['password']) < 8 || strlen($request['password']) > 16) {
      return false;
    }
    if (is_wp_error($user)) {
      return $user;
    }
    $user_id  = get_user_by('login', $login)->ID;
    $password = $request['password'];
    wp_set_password($password, $user_id);
    update_field('is_verified', true, "user_$user_id");
    // Clear facebook id?
    if (!empty($fields)) {
      foreach ($fields as $f) {
        if ($f['isAcf']) {
          update_field($f['key'], $f['value'], "user_$user_id");
        } else {
          update_user_meta($user_id, $f['key'], $f['value']);
        }
      }
    }
    // update_user_meta($user_id, 'facebook_id', NULL);
    return [
      'success' => true,
      'fields' => $fields
    ];
  }

  public static function update(WP_REST_Request $request)
  {
    /*
      fields: [
        {
          key: 'x',
          value: 'y',
          isAcf: true / false
        }
      ]
    */
    $fields = $request['fields'];
    if (!$fields) return ['success' => false, 'error' => 'No fields provided.'];
    $current_user = wp_get_current_user();
    $userId      = $current_user->ID;
    foreach ($fields as $f) {
      if ($f['isAcf']) {
        update_field($f['key'], $f['value'], "user_$userId");
        if ($f['key'] === 'newsletter_enabled') {
          if ($f['value']) {
            $current_user->remove_role('unsubscriber');
          } else {
            $current_user->add_role('unsubscriber');
          }
        }
      } else {
        update_user_meta($userId, $f['key'], $f['value']);
      }
    }
    return [
      'success' => true,
      'data' => AponUserAuthCore::getUser()
    ];
  }

  public static function createUserByEmailAndPassword(WP_REST_Request $request)
  {
    $username = sanitize_user($request['email']);
    $password = $request['password'];
    $emailExists = email_exists($username);
    if ($emailExists) {
      return [
        'success' => false,
        'error_code' => 'email_exists',
        'error_message' => 'Email already exists.'
      ];
    }
    $user_data = array(
      'user_login' => $username,
      'user_pass'  => $password,
      'user_email' => $username,
      'first_name' => $request['firstName'],
      'last_name'  => $request['lastName'],
    );
    $user = wp_insert_user($user_data);
    if (is_wp_error($user)) {
      return [
        'success' => false,
        'error_code' => $user,
        'error_message' => $user
      ];
    }
    return [
      'success' => true
    ];
  }

  public static function createUserByEmail(WP_REST_Request $request)
  {
    $username = sanitize_user($request['email']);
    $password = AponUserAuthCore::generateRandomString();
    // Check if email exists
    $emailExists = email_exists($username);
    if ($emailExists) {
      return [
        'success' => false,
        'error_code' => 'email_exists',
        'error_message' => 'Email already exists.'
      ];
    }

    $user_data = array(
      'user_login' => $username,
      'user_pass'  => $password,
      'user_email' => $username,
      'first_name' => $request['firstName'],
      'last_name'  => $request['lastName'],
    );
    $user = wp_insert_user($user_data);

    // 
    update_user_meta($user, 'billing' . '_phone', $request['phoneNumber']);

    if (is_wp_error($user)) {
      return [
        'success' => false,
        'error_code' => $user,
        'error_message' => $user
      ];
    }
    $origin = $request->get_header('origin');
    return AponUserAuthCore::sendResetPasswordViaEmail(['email' => $username, 'subject' => 'Please complete your account registration!', 'origin'], $origin, true);
  }

  public static function sendResetPasswordViaEmail($request, $origin, $isNew = false)
  {
    // URL that sent this request
    $email  = $request['email'];
    $subject = $request['subject'] ?? 'You have requested a password reset';
    $setPasswordURL = $request['setPasswordURL'] ?? '/auth/set-password';
    if (email_exists($email)) {
      $user = get_user_by('email', $email);
      // Check if registered via Facebook
      $is_facebook_user = get_user_meta($user->ID, 'facebook_id', true);
      if ($is_facebook_user) {
        return [
          'success'       => false,
          'error_code'    => 'already_facebook_user',
          'error_message' => 'Already a facebook user. Please login using Facebook',
        ];
      }

      $password_reset_key = get_password_reset_key($user);
      $to                 = $email;
      $message            = 'Dear user,<br><br>Please proceed to the link below.<br><br><a target="_blank" href="' . $origin . $setPasswordURL . '?login=' . $user->user_login . '&key=' . $password_reset_key . '&new=' .  $isNew . '">Click here</a><br><br>Thank you';
      $headers            = array('Content-Type: text/html; charset=UTF-8');
      wp_mail($to, $subject, $message, $headers);
      return [
        'success' => true,
        'userId' => $user->ID
      ];
    } else {
      return [
        'success'       => false,
        'error_code'    => 'email_not_found',
        'error_message' => 'Email not found',
      ];
    }
  }
}

<?php
/*
  Managed by AponUserAuthCore
*/
add_action('rest_api_init', function () {
  // Get User
  register_rest_route('v2', '/auth/social', array(
    'methods'  => 'POST',
    'permission_callback' => function () {
      return !is_user_logged_in();
    },
    'callback' => function ($x) {
      return AponUserAuthCore::registerLoginSocial($x);
    },
  ));
});

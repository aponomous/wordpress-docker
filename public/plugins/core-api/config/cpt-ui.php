<?php
// 

function generateCPTUI($_json = null)
{
  if (!file_exists(__DIR__ . '/cpt.json')) return;
  $string = file_get_contents(__DIR__ . '/cpt.json');
  if (!$_json) {
    $json = (json_decode($string, true));
  } else {
    $json = $_json;
  }
  foreach ($json['items'] as $value) {
    $labels = [
      'name' => __($value['label'] . 's', 'api'),
      'singular_name' => __($value['label'], 'api'),
      'all_items' => __('ดูทั้งหมด', 'api'),
      'add_new' => __('เพิ่ม', 'api'),
      'add_new_item' => __('เพิ่ม ' . $value['label'], 'api'),
      'edit_item' => __('แก้ไข ' . $value['label'], 'api'),
    ];
    $supports = ['title', 'revisions', 'custom-fields', 'author'];
    if (isset($value['comments'])) {
      $supports[] = 'comments';
    }
    $args = [
      'label' => __($value['label'], 'api'),
      'labels' => $labels,
      'public' => true,
      'show_in_rest' => false,
      'show_in_menu' => true,
      'show_in_nav_menus' => true,
      'capability_type' => 'post',
      'rewrite' => [
        'slug' => $value['slug'],
        'with_front' => true
      ],
      'menu_icon' => 'dashicons-arrow-right',
      'supports' => $supports,
      // Must be false in order for Sorting to work
      'hierarchical' => false,
    ];
    if (isset($value['category'])) $args['taxonomies'] = ['category'];
    register_post_type($value['slug'], $args);
  }
}

add_action('init', 'generateCPTUI');

<?php
// 
function my_customize_rest_cors()
{
  remove_filter('rest_pre_serve_request', 'rest_send_cors_headers');
  add_filter('rest_pre_serve_request', function ($value) {
    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN'] . '');
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Expose-Headers: Link');
    header('Access-Control-Allow-Headers: authentication, lang, fe-version, X-Requested-With, x-xsrf-token, content-type');
    header('Access-Control-Max-Age: 86400');
    return $value;
  });
}
add_action('rest_api_init', 'my_customize_rest_cors', 15);

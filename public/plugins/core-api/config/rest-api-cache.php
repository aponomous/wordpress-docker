<?php

function wprc_add_acf_posts_endpoint($allowed_endpoints)
{
  if (!isset($allowed_endpoints['v1'])) {
    $allowed_endpoints['v1'][] = 'cpt';
    $allowed_endpoints['v1'][] = 'meta';
    $allowed_endpoints['v1'][] = 'uwae-meta';
  }
  return $allowed_endpoints;
}

function wprc_determine_object_type($object_type, $cache_key, $data, $uri)
{
  // Do your magic here
  $object_type = 'custom_rest_cache';
  // Do your magic here
  return $object_type;
}

function wprc_flush_cache()
{
  if (is_plugin_active('wp-rest-cache/wp-rest-cache.php')) {
    \WP_Rest_Cache_Plugin\Includes\Caching\Caching::get_instance()->delete_object_type_caches('custom_rest_cache');
  }
  return true;
}

add_filter('wp_rest_cache/allowed_endpoints', 'wprc_add_acf_posts_endpoint', 10, 1);
add_filter('wp_rest_cache/determine_object_type', 'wprc_determine_object_type', 10, 4);
add_action('save_post', 'wprc_flush_cache', 10, 5);

<?php
// 
function my_acf_init()
{
  acf_update_setting('google_api_key', 'AIzaSyDZT6Pd5hRiz4jK6cBP5sCYcb64JeQXYwg');
  remove_filter('acf_the_content', 'wpautop');
}
add_action('acf/init', 'my_acf_init');


function wpb_change_title_text($title)
{
  // $screen = get_current_screen();
  return 'กรุณาพิมพ์ชื่อเป็นภาษาอังกฤษเพื่อกำหนด Slug';
}
add_filter('enter_title_here', 'wpb_change_title_text');


/**
 * Modify url base from wp-json to 'api'
 */
add_filter('rest_url_prefix', 'modifyRestAPISlug');

function modifyRestAPISlug($slug)
{
  return 'api';
}

function add_meta_keys_to_revision($keys)
{
  $keys[] = 'content';
  return $keys;
}
add_filter('wp_post_revision_meta_keys', 'add_meta_keys_to_revision');

// https://wordpress.stackexchange.com/questions/44748/tinymce-advanced-newline-problem
function change_mce_options($init)
{
  $init['forced_root_block'] = false;
  $init['force_br_newlines'] = true;
  $init['force_p_newlines'] = false;
  $init['convert_newlines_to_brs'] = true;
  return $init;
}
add_filter('tiny_mce_before_init', 'change_mce_options');

$groups = apply_filters('acf/get_field_groups', []);
if (is_array($groups)) {
  foreach ($groups as $group) {
    $fields = apply_filters('acf/field_group/get_fields', [], $group['id']);
    return $fields;
  }
}

// https://www.admincolumns.com/documentation/filter-reference/acp-editing-save_value/
function my_acp_editable_ajax_column_save_value($value, $column, $id)
{
  // Possibly modify $value
  return $value;
}
add_filter('acp/editing/save_value', 'my_acp_editable_ajax_column_save_value', 10, 3);

add_filter('lostpassword_url',  'my_lostpassword_url', 10, 0);
function my_lostpassword_url()
{
  return '/auth/forgot-password';
}

// https://betternotificationsforwp.com/documentation/compatibility/support-plugins-front-end-forms/
add_filter('bnfw_trigger_insert_post', '__return_true');

// https://www.cyberciti.biz/faq/php-wordpress-change-post-url-via-preview_post_link-filter/
// function nixcraft_preview_link()
// {
//   $id = get_the_ID();
//   $mynewpurl = "/preview/$id";
//   return "$mynewpurl";
// }
// add_filter('preview_post_link', 'nixcraft_preview_link');

function custom_image_size()
{
  // Set default values for the upload media box
  update_option('image_default_size', 'full');
}
add_action('after_setup_theme', 'custom_image_size');
// https://onlinemediamasters.com/disable-wordpress-heartbeat-api/

add_filter('auto_update_plugin', '__return_false');

add_action('admin_footer', function () {
?>
  <script>
    if (typeof commonL10n === 'undefined') {
      var commonL10n = {
        dismiss: 'Dismiss'
      };
    }
  </script>
<?php
}, 100);

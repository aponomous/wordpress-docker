<?php
// 
add_action('rest_api_init', function () {
  register_rest_route('v1', '/user/data', [
    'methods'  => 'GET',
    'callback' => function ($x) {
      return _OutboundAPI_::getUserData($x);
    },
    'callback_permission' => function () {
      return current_user_can('administrator');
    }
  ]);
});

class _OutboundAPI_
{
  public static function getUserData($data)
  {
    // WP_User_Query arguments
    $args = array(
      'number'         => $data['perPage'] ?? 300,
      'count_total'    => true,
      'fields'         => array('id'),
      'paged'          => $data['page'] ?? 1
    );

    // The User Query
    $query = new WP_User_Query($args);
    $users = (array) $query->get_results();
    $total = (int) $query->get_total();
    return [
      'users' => wp_list_pluck($users, 'id'),
      'totalUsers' => $total
    ];
  }
}

<?php
// 
add_action('rest_api_init', function () {
  register_rest_route('v3', '/cpt', array(
    'methods'  => 'GET',
    'callback' => function ($x) {
      return _CPTV3_::list($x);
    },
  ));
});

class _CPTV3_
{
  public static function list($data)
  {
    $postType = is_array($data['type']) ? $data['type'] : explode(',', $data['type']);
    if (!isset($postType))
      return [
        'error' => 'cpt not provided'
      ];
    $isFull = $data['full'] ?? false;
    $author = $data['author'];
    $metaQueryRelation = $data['metaQueryRelation'] ?? 'OR';
    $relatedPostTax = $data['relatedPostTax'] ?? false;
    $relatedContentPostNum = $data['relatedContentPostNum'] ?? 4;
    $status = $data['status'] ?? 'publish';
    $order = $data['order'];
    $metaQuery = $data['metaQuery'];
    $metaKey = $data['metaKey'];
    $metaQueryClauses = $data['metaQueryClauses'] ?? [];
    $orderBy = $data['orderBy'];
    $page = $data['page'] ?? 1;
    $perPage = $data['perPage'];
    $slug = $data['slug'];
    $dateQuery = $data['dateQuery'];
    $excludeSticky = $data['excludeSticky'];
    $sticky = $data['sticky'];
    if ($data['include']) $include = is_array($data['include']) ? $data['include'] : [$data['include']];
    if ($data['includeSlug']) $includeSlug = is_array($data['includeSlug']) ? $data['includeSlug'] : [$data['includeSlug']];
    if ($data['exclude']) $exclude = is_array($data['exclude']) ? $data['exclude'] : [$data['exclude']];
    $lang = $data['lang'] ?? 'th';
    $categories = $data['categories'];
    if ($data['taxonomies'] && $data['taxonomies'][0]) $taxonomies = is_array($data['taxonomies']) ? $data['taxonomies'] : [$data['taxonomies']];
    $s = $data['s'] ?? '';
    $_arg              = [
      'post_status'    => $status,
      'posts_per_page' => 20,
      'paginate'       => true,
      'post_type'      => $postType
    ];
    if (
      is_user_logged_in() && isset($author) && ((int) $author === get_current_user_id())
    ) {
      $_arg['author'] = $author;
      if (isset($status)) $_arg['post_status'] = $status;
    }
    if (isset($categories)) $_arg['category__in'] = $categories;
    /*
    [{slug: String, terms: Array}]
    */
    if (isset($taxonomies)) {
      $_arg['tax_query'] = [
        'relation' => isset($data['taxonomyRelation']) ? $data['taxonomyRelation'] : 'AND'
      ];
      foreach ($taxonomies as $taxonomy) {
        $_arg['tax_query'][] = [
          'taxonomy' => $taxonomy[0],
          'field' => $taxonomy[2] ? $taxonomy[2] : 'term_id',
          'terms' => $taxonomy[1]
        ];
      }
    }
    if (isset($order)) {
      remove_all_filters('posts_orderby');
      $_arg['order'] = $order;
    } // ASC, DESC
    if (isset($metaKey)) $_arg['meta_key'] = $metaKey;
    if (isset($orderBy)) $_arg['orderby'] = $orderBy;
    if (isset($page)) $_arg['paged'] = (int) $page;
    if (isset($perPage)) $_arg['posts_per_page'] = (int) $perPage;
    if (isset($slug)) $_arg['name'] = $slug;
    if (isset($include)) {
      $_arg['post__in'] = $include;
      $_arg['orderby'] = 'post__in';
    }
    if (isset($includeSlug)) {
      $_arg['post_name__in'] = $includeSlug;
      $_arg['orderby'] = 'post_name__in';
    }
    if (isset($exclude)) {
      $_arg['post__not_in'] = $exclude;
    }
    if (isset($excludeSticky) && $excludeSticky === 'true') $_arg['post__not_in'] = get_option('sticky_posts');
    if (isset($sticky)) $_arg['post__in'] = get_option('sticky_posts');
    if (isset($s)) $_arg['s'] = $s;
    // https://developer.wordpress.org/reference/classes/wp_query/#date-parameters
    if (isset($dateQuery)) $_arg['date_query'] = $dateQuery;
    if (isset($metaQuery) && is_array($metaQuery)) {

      // THIS MIGHT CAUSE HUAHIN TO BREAK
      $_arg['meta_query']['relation'] = $metaQueryRelation;
      foreach ($metaQuery as $mq) {
        $_arg['meta_query'][] = [
          'key' => $mq[0], // 1
          'value' => $mq[1],
          'compare' => $mq[2] ?? '='
        ];
      }
    } else if (!getenv('SINGLE_LANG') && !in_array('custom_page', $postType)) {
      $_arg['meta_query'] = _CPT_UTIL_::filterLanguageQuery($lang);
    }
    foreach ($metaQueryClauses as $mqc) {
      $_arg['meta_query'][$mqc[0]] = [
        'key' => $mqc[1],
        'compare' => $mqc[3] ?? '='
      ];
      if ($mqc[2]) {
        $_arg['meta_query'][$mqc[0]]['value'] = $mqc[2];
      }
    }
    // If multiple keyword search
    // https://wordpress.stackexchange.com/questions/223707/how-to-support-multiple-search-terms-query-within-one-process
    $query = null;
    if (strlen(trim($_arg['s'])) > 0 && class_exists('SWP_Query')) {
      $query = new SWP_Query($_arg);
    } else {
      $query = new WP_Query($_arg);
    }
    $_posts = $query->posts;
    $posts = [];
    $rp_arg = [];
    foreach ($_posts as &$pv) {
      $acf = get_fields($pv->ID);
      $bundle =  _CPT_UTILV3_::generateDefaultFields($pv, $isFull);
      if ($relatedContentPostNum && isset($slug) || (isset($include) && count($include) <= 1) || (isset($includeSlug) && count($includeSlug) <= 1)) {
        // Increment View cannot be used due to caching server caching
        $rp_arg = [
          'post_type'      => $postType,
          'posts_per_page' => $relatedContentPostNum, // Number of related posts to display
          'orderby'        => 'rand',
          'post__not_in'   => array($pv->ID), // Ensure that the current post is not displayed
        ];
        if ($relatedPostTax) {
          if (is_array($relatedPostTax)) {
            $rp_arg['tax_query'] = ['relation' => 'AND'];
            foreach ($relatedPostTax as &$rptx) {
              $terms = get_the_terms($pv->ID, $rptx);
              $term_ids = wp_list_pluck($terms, 'term_id');
              array_push($rp_arg['tax_query'], [
                'taxonomy' => $rptx,
                'field' => 'id',
                'operator' => 'IN',
                'terms' => $term_ids
              ]);
            }
          } else {
            $terms = get_the_terms($pv->ID, $relatedPostTax);
            $term_ids = wp_list_pluck($terms, 'term_id');
            $rp_arg['tax_query'] = [[
              'taxonomy' => $relatedPostTax,
              'field' => 'id',
              'operator' => 'IN',
              'terms' => $term_ids
            ]];
          }
        }
        // $postCategories = wp_get_post_categories($pv->ID);
        // if (count($postCategories) > 0) $rp_arg['category__in'] = $postCategories;
        if (!getenv('SINGLE_LANG') && !in_array('custom_page', $postType)) {
          $rp_arg['meta_query'] = _CPT_UTIL_::filterLanguageQuery($lang);
        }
        $rp_query          = new WP_Query($rp_arg);
        $rp_posts          = $rp_query->posts;
        $relatedPosts = [];
        foreach ($rp_posts as &$_rp) {
          $_rp_acf        = get_fields($_rp->ID);
          $_rp_bundle     = _CPT_UTILV3_::generateDefaultFields($_rp, $isFull);
          $relatedPosts[] = _CPT_UTIL_::mergeACF($_rp_acf, $lang, $postType, $_rp_bundle);
        }
        $bundle['relatedPosts'] = $relatedPosts;
      }
      $posts[] = _CPT_UTIL_::mergeACF($acf, $lang, $postType, $bundle);
    }
    $totalPosts = (int) $query->found_posts;
    $toReturn = [
      'posts'      => $posts,
      'page'       => (int) $page,
      'totalPages' => (int) $query->max_num_pages,
      'totalPosts' => $totalPosts
    ];
    if ($data['debug']) {
      $toReturn['_query'] = $query->query;
      $toReturn['_tax_query'] = $rp_arg;
    }
    $result = new WP_REST_Response($toReturn, 200);
    $result->set_headers([
      'Cache-Control' => 'public, max-age=3600, s-maxage=7200'
    ]);
    return $result;
  }
}

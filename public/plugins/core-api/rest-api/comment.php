<?php
// 
add_action('rest_api_init', function () {
  register_rest_route('v1', '/comment(?:/(?P<id>\d+))?', [
    [
      'methods'  => 'GET',
      'callback' => function ($x) {
        return _Comment_::listComment($x);
      }
    ],
    [
      'methods' => 'POST',
      'callback' => function ($x) {
        return _Comment_::addComment($x);
      },
      'permission_callback' => function () {
        return is_user_logged_in();
      }
    ],
    [
      'methods' => 'PUT',
      'callback' => function ($x) {
        return 'put';
      },
      'permission_callback' => function () {
        return is_user_logged_in();
      }
    ],
    [
      'methods' => 'DELETE',
      'callback' => function ($x) {
        return _Comment_::deleteComment($x);
      },
      'args' => [
        'id'
      ],
      'permission_callback' => function () {
        return is_user_logged_in();
      }
    ]
  ]);
});

class _Comment_
{
  public static function listComment($data)
  {
    $postId = $data['postId'];
    $args = [
      'post_id' => $postId
    ];
    $comments = get_comments($args);
    if (class_exists('AponUserAuthCore')) {
      foreach ($comments as &$comment) {
        $author_obj = get_user_by('id', $comment->user_id);
        $comment->author_image = AponUserAuthCore::extractAvatarImage($author_obj->user_login, $comment->user_id);
        $comment->custom_fields = get_comment_meta($comment->comment_ID, 'meta', false);
      }
    }
    return $comments;
  }
  public static function addComment(WP_REST_Request $request)
  {
    $agent = $_SERVER['HTTP_USER_AGENT'];
    $user = wp_get_current_user();
    $postId = $request['postId'];
    // $message = strip_tags($request['message'], '<strong><em><img>');
    $message = $request['message'];
    $meta = $request['meta'];
    $commentData = [
      'comment_post_ID'      => $postId,             // To which post the comment will show up.
      'comment_author'       => $user->first_name ? "$user->first_name $user->last_name" : $user->display_name,     // Fixed value - can be dynamic.
      // 'comment_author_email' => $user->user_email, // Fixed value - can be dynamic.
      'comment_author_IP'    => __getIP(),
      'comment_content'      => $message, // Fixed value - can be dynamic.
      'comment_type'         => '',                    // Empty for regular comments, 'pingback' for pingbacks, 'trackback' for trackbacks.
      'comment_agent' => $agent,
      'comment_parent'       => 0,                     // 0 if it's not a reply to another comment; if it's a reply, mention the parent comment ID here.
      'user_id'              => $user->ID,     // Passing current user ID or any predefined as per the demand.,
    ];
    $commentId = wp_new_comment($commentData);
    //
    if ($meta) {
      update_comment_meta($commentId, 'meta', $meta);
    }
    // 
    return $commentId;
  }
  public static function deleteComment($data)
  {
    $userId = get_current_user_id();
    $commentId = $data['id'];
    if (!$commentId) return ['success' => false, 'error' => 'no $commentId provided'];
    $comment = get_comment($commentId);
    $authorId = $comment->user_id;
    if ((int) $userId !== (int) $authorId) {
      return ['success' => false, 'error' => 'not a comment owner'];
    }
    $res = wp_delete_comment($commentId);
    return ['success' => true];
  }
}

function __getIP()
{
  foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
    if (array_key_exists($key, $_SERVER) === true) {
      foreach (array_map('trim', explode(',', $_SERVER[$key])) as $ip) {
        if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
          return $ip;
        }
      }
    }
  }
}

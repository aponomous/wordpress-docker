<?php
// 
class _CPT_UTIL_
{
  /*
      Multi language
      getenv('SINGLE_LANG')
      ถ้าไม่มีภาษาที่ request ก็ไม่ต้อง query
      
      post_type ต้องไม่ใช่ custom_page
      getenv('SINGLE_LANG') 

      'meta_query' => [
        'relation' => 'OR',
        [
          'key' => $lang . '_info_title',
          'compare' => 'EXISTS'
        ]
      ]
    */
  public static function filterLanguageQuery($lang)
  {
    return [
      'relation' => 'OR',
      [
        'key' => $lang . '_info_title',
        'value' => ['', null],
        'compare' => 'NOT IN'
      ],
      [
        'key' => $lang . '_title',
        'value' => ['', null],
        'compare' => 'NOT IN'
      ]
    ];
  }
  public static function mergeACF($acf, $lang, $_postType, $bundle)
  {
    $postType = is_array($_postType) ? $_postType : [$_postType];
    if (isset($acf[$lang]) && in_array('custom_page', $postType) === false) {
      $merged = isset($acf['general']) ? array_merge($bundle, $acf['general'], $acf[$lang]) : array_merge($bundle, $acf[$lang]);
      return $merged;
      // return _CPT_UTIL_::sanitizeACF($merged);
    } else if (in_array('custom_page', $postType) === true) {
      $merged = isset($acf['general']) ? array_merge($bundle, $acf['general'], isset($acf[$lang]) ? $acf[$lang] : []) : array_merge($bundle, isset($acf[$lang]) ? $acf[$lang] : []);
      return $merged;
      // return _CPT_UTIL_::sanitizeACF($merged);
    } else if ((isset($acf[get_option('default_language')]) && !isset($acf[$lang])) || getenv('SINGLE_LANG')) {
      $merged = array_merge($bundle, $acf);
      return $merged;
      // return _CPT_UTIL_::sanitizeACF($merged);
    }
    return null;
  }
  public static function generateDefaultFields($post, $isFull = false)
  {
    $id = $post->ID;
    $authorId = $post->post_author;
    $_taxonomies = get_taxonomies();
    $_views = get_field('view_count', $id);
    $views = $_views ? (int) $_views : 0;
    $_shares = get_field('share_count', $id);
    $shares = $_shares ? (int) $_shares : 0;
    $comment_count = $post->comment_count ? (int) $post->comment_count : 0;
    $seo = get_field('seo', $id) ?? [
      'meta_title' => $post->post_title
    ];
    $defaultFields = [
      '_id' => $id,
      '_seo' => $seo,
      '_title' => $post->post_title,
      '_slug' => $post->post_name,
      '_type' => $post->post_type,
      '_view_count' => $views,
      '_terms' => wp_get_post_terms($id, array_keys($_taxonomies)),
      '_is_sticky' => is_sticky($id),
      '_date_created' => $post->post_date,
    ];
    $extraFields = [
      '_date_modified' => $post->post_modified,
      '_status' => $post->post_status,
      '_share_count' => $shares,
      '_comment_count' => $comment_count,
      '_can_comment' => comments_open($id),
      '_is_your_post' => get_current_user_id() == $authorId,
      '_author' => [
        'id' => $authorId,
        'image' => get_avatar_url($authorId),
        'bio' => get_the_author_meta('description', $authorId),
        'name' => get_the_author_meta('first_name', $authorId) . ' ' . get_the_author_meta('last_name', $authorId)
      ]
    ];
    if ($isFull) return array_merge($defaultFields, $extraFields);
    return $defaultFields;
  }
  public static function sanitizeACF($post)
  {
    $_post = $post;
    foreach ($_post as $key => $value) {
      // Cleanup blank string values
      if ($value === '') $_post[$key] = null;
      // Cleanup unused image fields
      if (is_array($value)) {
        foreach ($value as $_key => $_value) {
          if ($_value === '') $_post[$key][$_key] = null;
        }
        // If not associative array [{}, {}, {}, ...]
        // 
        if (isset($value['type']) && $value['type'] == 'image') {
          $_post[$key] = [
            'url' => $value['sizes']['large'],
            'sizes' => [
              'thumbnail' => $value['sizes']['thumbnail'],
              'medium' => $value['sizes']['medium'],
              'large' => $value['sizes']['large']
            ]
          ];
        }
      }
    }
    return $_post;
  }
}

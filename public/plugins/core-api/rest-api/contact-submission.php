<?php
// 
add_action('rest_api_init', function () {
  register_rest_route('v1', '/contact-submission', array(
    'methods'  => 'POST',
    'callback' => function ($x) {
      return _ContactSubmission_::create($x);
    },
  ));
});

class _ContactSubmission_
{
  // https://betternotificationsforwp.com/documentation/compatibility/using-bnfw-with-the-wp-rest-api/
  static function create(WP_REST_Request $request)
  {
    $taxonomy = $request['taxonomy'];
    $senderEmail = $request['senderEmail'];
    $name = $request['name'];
    $phone = $request['phone'];
    $message = $request['message'];
    $postId = wp_insert_post([
      'post_title' => wp_strip_all_tags($senderEmail) . '_' . time(),
      'post_type' => 'contact_submission',
      'post_status' => 'draft',
      'post_author' => 1
    ]);
    update_field('sender_email', $senderEmail, $postId);
    update_field('name', $name, $postId);
    if ($phone) {
      update_field('phone', $phone, $postId);
    }
    update_field('message', $message, $postId);
    if ($taxonomy) {
      wp_set_object_terms($postId, $taxonomy[1], $taxonomy[0]);
    }
    sleep(2);
    wp_update_post([
      'ID' => $postId,
      'post_status' => 'publish'
    ]);

    return $postId;
  }
}

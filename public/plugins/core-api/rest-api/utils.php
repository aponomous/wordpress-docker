<?php

add_action('run_custom_remote_get', function ($url) {
  return wp_remote_get($url, ['timeout' => 1200]);
});

class _Utils_
{
  public function getHttpUrl($url, $params = null, $args = null)
  {
    $response = wp_remote_get($url, $args);
    if (is_array($response)) {
      return json_decode($response['body']);
    }
    return false;
  }
  public function incrementView($postId)
  {
    $count = (int) get_field('views', $postId);
    $count++;
    update_field('views', $count, $postId);
    return;
  }
  public function isAssoc(array $arr)
  {
    if (array() === $arr) return false;
    return array_keys($arr) !== range(0, count($arr) - 1);
  }
}

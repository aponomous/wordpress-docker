<?php
// 
add_action('rest_api_init', function () {
  // Init
  register_rest_route('v3', '/meta', array(
    'methods'  => 'GET',
    'callback' => function ($x) {
      return _MetaV3_::list($x);
    },
  ));
});

class _MetaV3_
{
  public static function list($data)
  {
    // Get Terms
    $loadTerms = $data['terms'];
    $numTaxTerm = $data['numTerm'] ?? 20;
    $toReturn = [
      'meta' => get_fields('options'),
      'taxonomies' => _MetaV3_::listTaxonomies(['numTerm' => $numTaxTerm]),
    ];
    if ($loadTerms) {
      $toReturn['terms'] = _CPT_::list(['type' => 'term', 'perPage' => 300])->data->posts;
    }
    $result = new WP_REST_Response($toReturn, 200);
    $result->set_headers([
      'Cache-Control' => 'public, max-age=3600, s-maxage=7200'
    ]);
    return $result;
  }
  public static function listTaxonomies($data)
  {
    // $slug = null, $numTerm
    $args = [];
    $slug = $data['slug'] ?? null;
    $numTerm = $data['numTerm'] ?? null;
    if ($slug) $args = ['name' => $slug];
    $_taxonomies = get_taxonomies($args, 'objects');
    $taxonomies = [];
    foreach ($_taxonomies as $key => &$value) {
      if ($value->name == 'category') continue;
      $_terms = get_terms($value->name, [
        'orderby' => $value->name === 'custom_tag' ? 'count' : '',
        'order' => $value->name === 'custom_tag' ? 'DESC' : '',
        'number' => absint($numTerm),
        'hide_empty' => false,
      ]);
      if (count($_terms) > 0) {
        $terms = [];
        foreach ($_terms as $tv) {
          $_tv = $tv;
          unset($_tv->term_group);
          unset($_tv->taxonomy);
          unset($_tv->term_taxonomy_id);
          unset($_tv->filter);
          $term_acf = get_fields($value->name . '_' . $_tv->term_id);
          $terms[] = array_merge((array) $term_acf, (array) $_tv);
        }
        $taxonomies[$value->name] = [
          'title' => $value->label,
          'slug' => $value->name,
          'terms' => $terms
        ];
      }
    }
    if ($slug) return $taxonomies[0];
    return $taxonomies;
  }
}

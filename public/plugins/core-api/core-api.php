<?php

/**
 * Plugin Name:       Core API
 * Version:           1.0.0
 */

require_once __DIR__ . '/config/rest-api-cache.php';
require_once __DIR__ . '/config/default.php';
require_once __DIR__ . '/config/cors.php';
require_once __DIR__ . '/config/cpt-ui.php';
require_once __DIR__ . '/config/FacebookDebugger.php';
require_once __DIR__ . '/rest-api/index.php';
require_once __DIR__ . '/rest-api/outbound-api.php';

<?php

// 
add_action('rest_api_init', function () {
  register_rest_route('v1', '/tpq-contact-submission', array(
    'methods'  => 'POST',
    'callback' => function ($x) {
      return _TPQContactSubmission_::create($x);
    },
  ));
});

class _TPQContactSubmission_
{
  // https://betternotificationsforwp.com/documentation/compatibility/using-bnfw-with-the-wp-rest-api/
  static function create(WP_REST_Request $request)
  {
    $form = $request['form'];
    $postId = wp_insert_post([
      'post_title' => wp_strip_all_tags($form['email']) . '_' . time(),
      'post_type' => 'contact_submission',
      'post_status' => 'draft',
      'post_author' => 1
    ]);
    foreach ($form as $field => $value) {
      update_field($field, $value, $postId);
    }
    sleep(2);
    wp_update_post([
      'ID' => $postId,
      'post_status' => 'publish'
    ]);

    return $postId;
  }
}

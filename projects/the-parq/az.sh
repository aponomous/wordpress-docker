# Copy custom-post-types
cp ./cpt.json ../../public/plugins/core-api/config/
# 
docker build -t fivelab.azurecr.io/the-parq-wp:latest -f ./Dockerfile ../../
docker push fivelab.azurecr.io/the-parq-wp:latest
# Remove custom-post-types
rm ../../public/plugins/core-api/config/cpt.json
# 
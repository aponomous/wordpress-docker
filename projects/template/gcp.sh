# Copy custom-post-types
cp ./cpt.json ../../public/plugins/core-api/config/
# 
gcloud config set project low-traffic
gcloud config set builds/use_kaniko False
gcloud builds submit --tag gcr.io/low-traffic/wp-test:v1 ../../
# Remove custom-post-types
rm ../../public/plugins/core-api/config/cpt.json
# gcloud compute instances create-with-container wp-test \
  # --machine-type=e2-micro \
gcloud compute instances update-container wp-test \
  --container-image gcr.io/low-traffic/wp-test:v1 \
  --container-restart-policy on-failure \
  --container-privileged \
  --container-env-file ./env.list \
# Copy custom-post-types
cp ./cpt.json ../../public/plugins/core-api/config/
# 
docker build -t fivelab.azurecr.io/medpark-cms:latest ../../
docker push fivelab.azurecr.io/medpark-cms:latest
# Remove custom-post-types
rm ../../public/plugins/core-api/config/cpt.json
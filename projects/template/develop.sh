# Remove custom-post-types
rm ../../public/plugins/core-api/config/cpt.json
# Copy custom-post-types
cp ./cpt.json ../../public/plugins/core-api/config/
# Copy plugins
# cp ./plugins.txt ../../public/plugins/
# Build docker image
docker build -t apon/local-wp ../../
# Remove custom plugins
# Run docker
docker run -p 127.0.0.1:8080:80/tcp \
  --name test \
  -it --rm \
  --env-file ./env.list \
  apon/local-wp
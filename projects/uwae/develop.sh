# Remove custom-post-types
rm ../../public/plugins/core-api/config/cpt.json
# Copy custom-post-types
cp ./cpt.json ../../public/plugins/core-api/config/
# Copy plugins
# cp ./plugins.txt ../../public/plugins/
# Build docker image
docker build --tag local-wordpress:v1 ../../
# Remove custom plugins
# Run docker
docker run -p 127.0.0.1:8080:80/tcp \
  -it --rm \
  --env-file ./env.list \
  local-wordpress:v1
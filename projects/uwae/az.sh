# Copy custom-post-types
cp ./cpt.json ../../public/plugins/core-api/config/
# 
docker build -t fivelab.azurecr.io/uwae-wp:latest -f ./Dockerfile ../../
docker push fivelab.azurecr.io/uwae-wp:latest
# Remove custom-post-types
rm ../../public/plugins/core-api/config/cpt.json
<?php
// 
function on_event_notification_published($ID, $post)
{
  // A function to perform actions when a post is published.
  if ($post->post_type == 'event_notification') {
    $authorId = $post->post_author;
    wp_remote_post('https://event-dispatcher.uwae.online', [
      'body' => wp_json_encode([
        'actionType' => 'EVENT_NOTIFICATION',
        'otherEarnerId' => $authorId,
        'postId' => $post->ID
      ])
    ]);
  }
}
add_action('publish_post',  'on_event_notification_published', 10, 2);

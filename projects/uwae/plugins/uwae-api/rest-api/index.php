<?php
// 
add_action('rest_api_init', function () {
  register_rest_route('v1', '/uwae-meta', [
    'methods'  => 'GET',
    'callback' => function ($x) {
      return _Uwae_::getMeta($x);
    },
  ]);
  register_rest_route('v1', '/user/data', [
    'methods'  => 'GET',
    'callback' => function ($x) {
      return _Uwae_::getUserData($x);
    },
  ]);
  register_rest_route('v1', '/user/data/point', [
    'methods'  => 'POST',
    'callback' => function ($x) {
      return _Uwae_::updatePoint($x);
    },
  ]);
  register_rest_route('v1', '/generate-persona-content', [
    'methods'  => 'POST',
    'callback' => function ($x) {
      return _Uwae_::generatePersonaContent();
    },
  ]);
  register_rest_route('v1', '/persona-content', [
    'methods'  => 'GET',
    'callback' => function ($x) {
      return _Uwae_::listPersonaContent($x);
    },
  ]);
  register_rest_route('v1', '/question-meta', [
    'methods'  => 'GET',
    'permission_callback' => function () {
      return isRole(['doctor', 'question_moderator']);
    },
    'callback' => function ($x) {
      return _Uwae_::listQuestionMeta($x);
    },
  ]);
  register_rest_route('v1', '/question', [
    'methods'  => 'GET',
    'callback' => function ($x) {
      return _Uwae_::listQuestion($x);
    },
  ]);
  register_rest_route('v1', '/question', [
    'methods'  => 'POST',
    'callback' => function ($x) {
      return _Uwae_::createQuestion($x);
    },
  ]);
  register_rest_route('v1', '/question/answer', [
    'methods'  => 'POST',
    'callback' => function ($x) {
      return _Uwae_::answer($x);
    },
  ]);
  register_rest_route('v1', '/question/assign', [
    'methods'  => 'PUT',
    'permission_callback' => function () {
      return isRole(['question_moderator']);
    },
    'callback' => function ($x) {
      return _Uwae_::assignDoctor($x);
    },
  ]);
  register_rest_route('v1', '/question/cancel-assign', [
    'methods'  => 'PUT',
    'permission_callback' => function () {
      return isRole(['question_moderator']);
    },
    'callback' => function ($x) {
      return _Uwae_::cancelAssignDoctor($x);
    },
  ]);
  register_rest_route('v1', '/favorite', [
    'methods' => 'PUT',
    'callback' => function ($x) {
      return _Uwae_::favorite($x);
    },
    'permission_callback' => function () {
      return is_user_logged_in();
    }
  ]);
  register_rest_route('v1', '/pin-activity', [
    'methods' => 'PUT',
    'callback' => function ($x) {
      return _Uwae_::pin($x);
    },
    'permission_callback' => function () {
      return is_user_logged_in();
    }
  ]);
});

class _Uwae_
{
  public static function getMeta($data)
  {
    // 
    return [
      'meta' => _Meta_::list($data),
      'ig_celebs' => _CPT_::list([
        'type' => 'ig_celeb',
        'perPage' => 300
      ]),
      'home' => _CPT_::list([
        'type' => 'custom_page',
        'slug' => 'home'
      ]),
    ];
  }

  public static function updatePoint(WP_REST_Request $request)
  {
    if (!$request['id']) return false;
    $_userId = $request['id'];
    $userId = "user_$_userId";
    update_field('point', [
      'available_points' => $request['availablePoints'] ?? 0,
      'month_minus_0' => $request['monthlyPoint']['month_minus_0'] ?? 0,
      'month_minus_1' => $request['monthlyPoint']['month_minus_1'] ?? 0,
      'month_minus_2' => $request['monthlyPoint']['month_minus_2'] ?? 0,
      'month_minus_3' => $request['monthlyPoint']['month_minus_3'] ?? 0,
      'month_minus_4' => $request['monthlyPoint']['month_minus_4'] ?? 0,
      'month_minus_5' => $request['monthlyPoint']['month_minus_5'] ?? 0,
      'updated_at' => current_time('mysql')
    ], $userId);
    return [
      'success' => true,
      'userId' => $_userId
    ];
  }
  public static function getUserData($data)
  {
    // WP_User_Query arguments
    $args = array(
      'role'           => 'Member',
      'number'         => $data['perPage'] ?? 300,
      'count_total'    => true,
      'fields'         => array('id'),
      'paged'          => $data['page'] ?? 1
    );

    // The User Query
    $query = new WP_User_Query($args);
    $users = (array) $query->get_results();
    $total = (int) $query->get_total();
    return [
      'users' => wp_list_pluck($users, 'id'),
      'totalUsers' => $total
    ];
  }
  public static function listPersonaContent($data)
  {
    $slug = $data['slug'];
    $post = get_page_by_path($slug, OBJECT, 'persona_content');
    $postIds = get_field('posts', $post->ID);
    $contents = _CPT_::list(['type' => 'content', 'include' => $postIds, 'isFull' => 1]);
    return $contents;
  }
  public static function generatePersonaContent()
  {
    $args = array(
      'numberposts' => -1,
      'post_type' => 'persona_content',
      // 'fields' => 'ids'
    );

    $personas = get_posts($args);
    foreach ($personas as $psn) {
      $isFrozen = get_field('is_frozen', $psn->ID);
      if ($isFrozen === true) continue;
      $contentsInOrder = [];
      // Find 6 random persona posts
      $_args = array(
        'numberposts' => 6,
        'orderby' => 'rand',
        'post_type' => 'content',
        'tax_query' => [[
          'taxonomy' => 'custom_tag',
          'field' => 'slug',
          'terms' => $psn->post_name
        ]],
        'fields' => 'ids'
      );
      $psnContentIds = get_posts($_args);
      // #ทุกวัย
      $_args = array(
        'numberposts' => 2,
        'orderby' => 'rand',
        'post_type' => 'content',
        'tax_query' => [[
          'taxonomy' => 'custom_tag',
          'field' => 'id',
          'terms' => 218
        ]],
        'fields' => 'ids'
      );
      remove_all_filters('posts_orderby');
      $genericContentIds = get_posts($_args);
      // #Recco
      $_args = array(
        'numberposts' => 2,
        'orderby' => 'rand',
        'post_type' => 'content',
        'tax_query' => [[
          'taxonomy' => 'custom_tag',
          'field' => 'slug',
          'terms' => 'recco'
        ]],
        'fields' => 'ids'
      );
      $reccoContentIds = get_posts($_args);
      $contentsInOrder = [$psnContentIds[0], $psnContentIds[1], $psnContentIds[2], $reccoContentIds[0], $genericContentIds[0], $genericContentIds[1], $reccoContentIds[1], $psnContentIds[3], $psnContentIds[4], $psnContentIds[5]];
      update_field('posts', $contentsInOrder, $psn->ID);
      wp_update_post(['ID' => $psn->ID]);
    }
    return [
      'success' => true,
      '$psn' => wp_list_pluck($personas, 'ID')
    ];
  }
  public static function listQuestionMeta($data)
  {
    $args = array(
      'role'           => 'doctor',
      'number'         => '100',
    );

    // The User Query
    $userQuery = new WP_User_Query($args);
    $_authors = $userQuery->get_results();
    $authors = [];
    foreach ($_authors as $author) {
      $userData = get_userdata($author->ID);
      $authors[] = [
        'id' => $author->ID,
        'displayName' => $userData->display_name,
        'email' => $userData->user_email,
        'image' => AponUserAuthCore::extractAvatarImage(null, $author->ID)
      ];
    }
    $totalUsers = (int) $userQuery->get_total;

    $statuses = [
      'draft' => 0,
      'pending' => 0,
      'publish' => 0
    ];

    foreach ($statuses as $key => &$value) {
      $query = new WP_Query([
        'post_type' => ['question'],
        'post_status' => [$key]
      ]);
      $statuses[$key] = $query->found_posts;
    }

    $toReturn = [
      'users'      => $authors,
      'totalUsers' => $totalUsers,
      'statuses' => $statuses,
      'timestamp'  => time(),
      '_query' => $userQuery->query,
    ];
    $result = new WP_REST_Response($toReturn, 200);
    // $result->set_headers([
    //   'Pragma' => 'cache',
    //   'Cache-Control' => 'public, max-age=3600, s-max-age=7200'
    // ]);
    return $result;
  }
  public static function listQuestion($data)
  {
    $isFull = $data['isFull'] ?? true;
    $postType = 'question';
    $belongToUser = $data['belongToUser'];
    // $author = $data['author'];
    $status = $data['status'];
    $order = $data['order'];
    $metaQuery = $data['metaQuery'];
    $orderBy = $data['orderBy'];
    $page = $data['page'] ?? 1;
    $perPage = $data['perPage'];
    $slug = $data['slug'];
    $excludeSticky = $data['excludeSticky'];
    $sticky = $data['sticky'];
    if ($data['include']) $include = is_array($data['include']) ? $data['include'] : [$data['include']];
    $lang = $data['lang'] ?? 'th';
    if ($data['taxonomies'] && $data['taxonomies'][0]) $taxonomies = is_array($data['taxonomies']) ? $data['taxonomies'] : [$data['taxonomies']];
    $s = $data['s'] ?? null;
    $_arg              = [
      'post_status'    => $status,
      'posts_per_page' => 20,
      'paginate'       => true,
      'post_type'      => $postType
    ];
    /*
      Logged in User: ['member', 'doctor', 'question_moderator']
    */
    if (!is_user_logged_in()) {
      $_arg['post_status'] = 'publish';
    } else {
      // Logged in
      if (isset($belongToUser) && $belongToUser == '1') {
        $_arg['author'] = get_current_user_id();
      }
      $_arg['post_status'] = $status ?? ['pending', 'draft', 'publish'];
    }

    if (isRole(['doctor'])) {
      if (!$include) {
        $_arg['meta_query'] = [
          [
            'key' => 'assigned_doctor',
            'value' => get_current_user_id(),
            'compare' => '=',
          ]
        ];
      }
      $_arg['post_status'] = ['pending', 'publish'];
    }

    if (isset($taxonomies)) {
      $_arg['tax_query'] = [
        'relation' => isset($data['taxonomyRelation']) ? $data['taxonomyRelation'] : 'AND'
      ];
      foreach ($taxonomies as $taxonomy) {
        $_arg['tax_query'][] = [
          'taxonomy' => $taxonomy[0],
          'field' => $taxonomy[2] ? $taxonomy[2] : 'term_id',
          'terms' => $taxonomy[1]
        ];
      }
    }
    if (isset($order)) {
      remove_all_filters('posts_orderby');
      $_arg['order'] = $order;
    } // ASC, DESC
    if (isset($orderBy)) $_arg['orderby'] = $orderBy;
    if (isset($page)) $_arg['paged'] = (int) $page;
    if (isset($perPage)) $_arg['posts_per_page'] = (int) $perPage;
    if (isset($slug)) $_arg['name'] = $slug;
    if (isset($include)) {
      $_arg['post__in'] = $include;
      $_arg['orderby'] = 'post__in';
    }
    if (isset($excludeSticky)) $_arg['post__not_in'] = get_option('sticky_posts');
    if (isset($sticky)) $_arg['post__in'] = get_option('sticky_posts');
    if (isset($s)) $_arg['s'] = $s;
    if (isset($metaQuery) && is_array($metaQuery)) {
      $isArrayOfInt = is_array($metaQuery[1]) && isset($metaQuery[3]);

      if ($isArrayOfInt) {
        $_arg['meta_query']['relation'] = 'OR';
        foreach ($metaQuery[1] as $val) {
          $_arg['meta_query'][] = [
            'key' => $metaQuery[0], // 1
            'value' => $val,
            'compare' => $metaQuery[2] ? $metaQuery[2] : '='
          ];
        }
      } else {
        $_arg['meta_query'] = [
          [
            'key' => $metaQuery[0],
            'value' => $metaQuery[1],
            'compare' => $metaQuery[2] ? $metaQuery[2] : '='
          ],
        ];
      }
    } else if (!getenv('SINGLE_LANG') && !in_array('custom_page', $postType)) {
      $_arg['meta_query'] = _CPT_UTIL_::filterLanguageQuery($lang);
    }

    $query = null;
    if ((isset($_arg['s']) && strlen(trim($_arg['s'])) > 0) && class_exists('SWP_Query')) {
      $query = new SWP_Query($_arg);
    } else {
      $query = new WP_Query($_arg);
    }
    $_posts = $query->posts;
    $posts = [];
    foreach ($_posts as &$pv) {
      $acf = get_fields($pv->ID);
      $bundle =  _CPT_UTIL_::generateDefaultFields($pv, $isFull);
      if (isset($slug) || (isset($include) && count($include) <= 1)) {
        // Increment View cannot be used due to caching server caching
        $rp_arg = [
          'post_type'      => ['question'],
          'posts_per_page' => 4, // Number of related posts to display
          'orderby' => 'rand',
          'post__not_in'   => array($pv->ID), // Ensure that the current post is not displayed
        ];
        $postCategories = wp_get_post_categories($pv->ID);
        if (count($postCategories) > 0) $rp_arg['category__in'] = $postCategories;
        $rp_query          = new WP_Query($rp_arg);
        $rp_posts          = $rp_query->posts;
        $relatedPosts = [];
        foreach ($rp_posts as &$_rp) {
          $_rp_acf        = get_fields($_rp->ID);
          $_rp_bundle           = _CPT_UTIL_::generateDefaultFields($_rp);
          $relatedPosts[] = _CPT_UTIL_::mergeACF($_rp_acf, $lang, $postType, $_rp_bundle);
        }
        $bundle['relatedPosts'] = $relatedPosts;
      }
      $posts[] = _CPT_UTIL_::mergeACF($acf, $lang, $postType, $bundle);
    }
    $totalPosts = (int) $query->found_posts;
    $toReturn = [
      'posts'      => $posts,
      'page'       => (int) $page,
      'totalPages' => (int) $query->max_num_pages,
      'totalPosts' => $totalPosts,
      'timestamp'  => time(),
      '_query' => $query->query,
      '_singular' => $query->is_singular
    ];
    $result = new WP_REST_Response($toReturn, 200);
    // $result->set_headers([
    //   'Pragma' => 'cache',
    //   'Cache-Control' => 'public, max-age=3600, s-max-age=7200'
    // ]);
    return $result;
  }
  public static function cancelAssignDoctor(WP_REST_Request $request)
  {
    $questionId = wp_strip_all_tags($request['questionId']);
    // Update custom field
    update_field('assigned_doctor', NULL, $questionId);
    update_field('doctor_name', NULL, $questionId);
    update_field('doctor_email', NULL, $questionId);
    update_field('doctor_token', NULL, $questionId);
    $args = [
      'ID' => $questionId,
      'post_status' => 'draft'
    ];
    $res = wp_update_post($args);
    if (is_wp_error($res)) {
      $errors = $res->get_error_messages();
      return [
        'success' => false,
        'error' => $errors
      ];
    }
    return [
      'success' => true,
      'data' => $res
    ];
  }
  public static function assignDoctor(WP_REST_Request $request)
  {
    $questionId = wp_strip_all_tags($request['questionId']);
    $doctorId = $request['doctorId'];
    $doctorName = wp_strip_all_tags($request['doctorName']);
    $doctorEmail = wp_strip_all_tags($request['doctorEmail']);

    if (!isset($doctorId)) {
      $user_data = array(
        'user_login' => $doctorEmail,
        'user_pass'  => $doctorEmail,
        'user_email' => $doctorEmail,
        'display_name' => $doctorName,
        'role' => 'doctor'
      );
      $user = wp_insert_user($user_data);
      if (is_wp_error($user)) {
        return [
          'success' => false,
          'error_code' => $user,
          'error_message' => $user
        ];
      }
      $doctorId = $user;
    }

    $token = NULL;
    if (class_exists('AAM_Service_Jwt')) {
      $jwt = AAM_Service_Jwt::getInstance()->issueToken($doctorId);
      $token = $jwt ? $jwt->token : 'no token';
    }
    // Update custom field
    update_field('assigned_doctor', $doctorId, $questionId);
    update_field('doctor_token', $token, $questionId);
    update_field('doctor_name', $doctorName, $questionId);
    update_field('doctor_email', $doctorEmail, $questionId);
    // Update post
    $args = [
      'ID' => $questionId,
      'post_status' => 'pending'
    ];
    $res = wp_update_post($args);
    if (is_wp_error($res)) {
      $errors = $res->get_error_messages();
      return [
        'success' => false,
        'error' => $errors
      ];
    }
    return [
      'success' => true,
      'data' => $res
    ];
  }
  public static function answer(WP_REST_Request $request)
  {
    $postId = wp_strip_all_tags($request['postId']);
    if (!$postId) {
      // TODO
      return [
        'success' => false,
        'error' => 'no $postId'
      ];
    }
    $message = wp_strip_all_tags($request['message']);
    update_field('answer', $message, $postId);
    $res = wp_update_post(['post_status' => 'publish', 'ID' => $postId]);
    if (is_wp_error($res)) {
      //the post is valid
      return [
        'success' => false,
        'error' => $res->get_error_message()
      ];
    }
    return [
      'success' => true
    ];
  }
  public static function createQuestion(WP_REST_Request $request)
  {
    $title = wp_strip_all_tags($request['title']);
    $description = wp_strip_all_tags($request['description']);
    $attachmentIds = $request['attachmentIds'];
    $newPost = array(
      'post_title'    => $title,
      'post_status'   => 'private',
      'post_type'     => 'question'
    );

    // Insert the post into the database.
    $postId = wp_insert_post($newPost);
    if (is_wp_error($postId)) {
      //the post is valid
      return [
        'success' => false,
        'error' => $postId->get_error_message()
      ];
    }
    update_field('description', $description, $postId);
    update_field('attachments', $attachmentIds, $postId);
    wp_update_post(['post_status' => 'draft', 'ID' => $postId]);
    return [
      'success' => true
    ];
  }
  public static function pin(WP_REST_Request $request)
  {
    $id = wp_strip_all_tags($request['postId']);
    if (!$id) {
      // TODO
      return [
        'success' => false,
        'error' => 'no $postId'
      ];
    }
    $userId = get_current_user_id();
    $pinList = get_field(
      'pinned_activities',
      "user_$userId"
    );
    if (!is_array($pinList)) {
      $pinList = [];
    }
    $existingIdIndex = NULL;
    for ($i = 0; $i < count($pinList); $i++) {
      $idValue = $pinList[$i]['id'];
      if ((int) $idValue === (int) $id) {
        $existingIdIndex = $i;
      }
    }
    if ($existingIdIndex === NULL) {
      // https://www.advancedcustomfields.com/resources/delete_row/
      add_row('pinned_activities', ['id' => $id], "user_$userId");
      // Get event `start_date`
      $startDate = get_field('general_start_date', $id);
      $title = get_field('general_title', $id);
      $content = get_field('general_content', $id);
      $slug = get_post_field('post_name', get_post($id));
      // Create `event_notification` post
      $newEventNotification = wp_insert_post([
        'post_type' => 'event_notification',
        'post_status' => 'future',
        'post_date' => date('Y-m-d H:i:s', strtotime($startDate) - (3 * 86400) + rand(1, 100)),
        'post_title' => "notify $id $userId"
      ]);
      update_field('event_start_date', $startDate, $newEventNotification);
      update_field('event_title', $title, $newEventNotification);
      update_field('event_content', $content, $newEventNotification);
      update_field('event_slug', $slug, $newEventNotification);
    } else {
      delete_row('pinned_activities', $existingIdIndex + 1, "user_$userId");
      // Delete `event_notification` post
      $existingPostId = get_posts(['name' => "notify-{$id}-{$userId}"]);
      if ($existingPostId && $existingPostId[0]) {
        wp_delete_post($existingPostId[0]->ID);
      }
    }
    $newPinList = get_field(
      'pinned_activities',
      "user_$userId"
    );
    return [
      'success' => true,
      'pinned_activities' => $newPinList
    ];
  }
  public static function favorite(WP_REST_Request $request)
  {
    $id = wp_strip_all_tags($request['postId']);
    if (!$id) {
      // TODO
      return [
        'success' => false,
        'error' => 'no $postId'
      ];
    }
    $userId = get_current_user_id();
    $favList = get_field(
      'favorited_contents',
      "user_$userId"
    );
    if (!is_array($favList)) {
      $favList = [];
    }
    $existingIdIndex = NULL;
    for ($i = 0; $i < count($favList); $i++) {
      $idValue = $favList[$i]['id'];
      if ((int) $idValue === (int) $id) {
        $existingIdIndex = $i;
      }
    }
    if ($existingIdIndex === NULL) {
      // https://www.advancedcustomfields.com/resources/delete_row/
      add_row('favorited_contents', ['id' => $id], "user_$userId");
    } else {
      delete_row('favorited_contents', $existingIdIndex + 1, "user_$userId");
    }
    $newPinList = get_field(
      'favorited_contents',
      "user_$userId"
    );
    return [
      'success' => true,
      'favorited_contents' => $newPinList
    ];
  }
}

function isRole($roles, $user_id = null)
{
  if ($user_id) $user = get_userdata($user_id);
  else $user = wp_get_current_user();
  if (empty($user)) return false;
  foreach ($user->roles as $role) {
    if (in_array($role, $roles)) {
      return true;
    }
  }
  return false;
}

function is_in_array_deep($array, $key, $key_value)
{
  $within_array = 'no';
  foreach ($array as $k => $v) {
    if (is_array($v)) {
      $within_array = is_in_array($v, $key, $key_value);
      if ($within_array == 'yes') {
        break;
      }
    } else {
      if ($v == $key_value && $k == $key) {
        $within_array = 'yes';
        break;
      }
    }
  }
  return $within_array;
}

function doSubmitNotification($userId, $postId, $actionType)
{
  // do something
  // otherEarnerId
  $res = wp_remote_post('https://event-dispatcher.uwae.online', [
    'body' => wp_json_encode([
      'actionType' => $actionType,
      'otherEarnerId' => $userId,
      'postId' => $postId
    ])
  ]);
  return $res;
}
add_action('activity_notification', 'doSubmitNotification');

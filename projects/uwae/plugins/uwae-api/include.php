<?php

function getUserTokenShortcodeInit()
{
  function getUserToken($atts = [], $content = null)
  {
    if (!$content) return 'no token';
    $email = do_shortcode($content);
    $user = get_user_by('email', $email);
    $token = AAM_Service_Jwt::getInstance()->issueToken($user->ID);
    return $token;
  }
  add_shortcode('get_user_token', 'getUserToken');
}

add_action('init', 'getUserTokenShortcodeInit');

// Limit access to media library (users can only see/select own media)
// https://wpdynamic.com/wordpress-developer/wordpress-code-snippets/how-to-restrict-wordpress-media-library-access-to-users-own-uploads/
add_filter('ajax_query_attachments_args', 'wpsnippet_show_current_user_attachments');
function wpsnippet_show_current_user_attachments($query)
{
  $user = wp_get_current_user();
  // Exclude role member
  if (in_array('editor', (array) $user->roles)) {
    //The user has the "author" role
    $query['author'] = 999;
    $args = ['role__not_in' => ['member']];
    $my_user_query = new WP_User_Query($args);
    // Get query results.
    $editors = $my_user_query->get_results();
    $edtitorArray = [];
    foreach ($editors as $editor) {
      array_push($edtitorArray, $editor->ID);
    }
    // $query['author__in'] = $edtitorArray;
    // $query['author__in'] = array_merge([118, 19, 18, 17, 16, 14], $edtitorArray);
    $query['author__in'] = [118, 19, 18, 17, 16, 14];
  }
  // 

  return $query;
}

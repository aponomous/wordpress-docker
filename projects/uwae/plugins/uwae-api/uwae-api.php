<?php

/**
 * Plugin Name:       Uwae-specific API
 * Version:           1.0.0
 */

require_once __DIR__ . '/rest-api/index.php';
require_once __DIR__ . '/include.php';
require_once __DIR__ . '/event-notification.php';

<?php
// 
add_action('rest_api_init', function () {
  register_rest_route('v1', '/celeb-scraper', array(
    'methods'  => 'POST',
    'callback' => function ($x) {
      return _CelebScraper_::batchScrape();
    },
  ));
});

class _CelebScraper_
{
  public static $url = 'https://insta-scrape-node-uwae-qptinmnofq-de.a.run.app/instagram/post/';

  public static function batchScrape()
  {
    // Get all celeb
    $celebs = get_posts([
      'numberposts' => 200,
      'post_type'   => 'ig_celeb',
    ]);
    foreach ($celebs as $celeb) {
      $celebId = $celeb->ID;
      $username = get_field('general_ig_username', $celebId);
      $hashtag = get_field('general_ig_hashtag', $celebId);
      _CelebScraper_::scrape($celebId, $username, $hashtag);
    }
    return true;
  }

  public static function scrape($celebId, $username, $hashtag)
  {
    //
    $response = wp_remote_get(_CelebScraper_::$url . $username . '/range?start=' . date('Y-m-d', strtotime('-1 months')), ['timeout' => 1200]);
    if (is_array($response)) {
      $response = json_decode($response['body']);
    }
    $posts = $response->posts ?? null;
    if (!$posts) return false;
    $storeRes = _CelebScraper_::storePosts($celebId, array_filter($posts, function ($p) {
      // return $p->is_video && $p->video_url;
      return true;
    }));
    return $storeRes;
  }
  private static function storePosts($celebId, $posts)
  {
    foreach ($posts as $post) {
      $update = [
        'post_id'         => $post->id,
        'taken_at_timestamp' => $post->taken_at_timestamp,
        'shortcode'       => $post->shortcode,
        'description'     => (isset($post->edge_media_to_caption) && isset($post->edge_media_to_caption->edges) && isset($post->edge_media_to_caption->edges[0])) ? $post->edge_media_to_caption->edges[0]->node->text : '',
        'like_count'      => (int) $post->edge_media_preview_like->count,
        'display_image'   => $post->display_url,
        'is_video'       => $post->is_video,
        'video_url'       => isset($post->video_url) ? $post->video_url : null,
        'children_images' => (array) _CelebScraper_::getChildrenImages($post),
        'owner'           => (array) $post->owner,
        'celeb'           => $celebId
      ];
      $hasPost = get_posts(array(
        'numberposts' => 1,
        'post_type'   => 'ig_celeb_post',
        'meta_key'    => 'post_id',
        'meta_value'  => $post->id,
        'post_status' => 'any',
      ));
      if (count($hasPost) === 1) {
        $post_id = $hasPost[0]->ID;
        _CelebScraper_::updateFields($post_id, $update);
      } else {
        $args = [
          'post_type'   => 'ig_celeb_post',
          'post_title'  => $post->shortcode,
          'post_status' => 'publish',
        ];
        $new_post_id = wp_insert_post($args);
        _CelebScraper_::updateFields($new_post_id, $update, true);
      }
    }
    return [
      'success' => true
    ];
  }
  private static function updateFields($id, $dataArray, $is_new = false)
  {
    foreach ($dataArray as $key => $value) {
      if ($is_new) {
        if ($key === 'taken_at_timestamp') {
          update_field($key, date("Y-m-d H:i:s", $value), $id);
        } else {
          update_field($key, $value, $id);
        }
      } else {
        if ($key === 'taken_at_timestamp') {
          update_field($key, date("Y-m-d H:i:s", $value), $id);
        }
        if (
          $key === 'display_image' ||
          $key === 'video_url' ||
          $key === 'is_video' ||
          $key === 'children_images' ||
          $key === 'like_count' ||
          $key === 'description'
        ) {
          update_field($key, $value, $id);
        }
      }
    }
    return wp_update_post([
      'ID' => $id,
    ]);
  }
  private static function getChildrenImages($post)
  {
    if (!isset($post->edge_sidecar_to_children)) {
      return null;
    }
    $toReturn = [];
    foreach ($post->edge_sidecar_to_children->edges as $edge) {
      $toReturn[] = [
        'url' => $edge->node->display_url,
      ];
    }
    return $toReturn;
  }
}

<?php
// 
add_action('rest_api_init', function () {
  register_rest_route('v1', '/product', array(
    'methods'  => 'GET',
    'callback' => function ($x) {
      return AponWCProduct::listProduct($x);
    },
  ));
  register_rest_route('v1', '/product/vendor', array(
    'methods'  => 'GET',
    'callback' => function ($x) {
      return AponWCProduct::listVendor($x);
    },
  ));
});

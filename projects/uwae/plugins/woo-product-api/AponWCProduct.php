<?php
//
class AponWCProduct
{
  private static function __getVendorDataByProductId($productId)
  {
    if (!class_exists('WC_Product_Vendors_Utils')) {
      return null;
    }
    $terms = wp_get_post_terms(absint($productId), WC_PRODUCT_VENDORS_TAXONOMY, ['fields' => 'all']);
    if (count($terms) <= 0) {
      return null;
    }
    $_vendor = $terms[0];
    $vendor_data = get_term_meta(absint($_vendor->term_id), 'vendor_data', true);
    $logo = !empty($vendor_data['logo']) ? $vendor_data['logo'] : '';
    $logo_image_url = wp_get_attachment_image_src($logo, 'medium');
    $vendor = [
      'id' => $_vendor->term_id,
      'name' => $_vendor->name,
      'slug' => $_vendor->slug,
      'logo' => $logo_image_url,
      'description' => $vendor_data['profile']
    ];
    return $vendor;
  }
  public static function listVendor()
  {
    if (!class_exists('WC_Product_Vendors_Utils')) {
      return null;
    }
    // $vendors = WC_Product_Vendors_Utils::get_vendors();
    $vendors = get_terms(WC_PRODUCT_VENDORS_TAXONOMY, array(
      'hide_empty' => false,
    ));
    foreach ($vendors as $v) {
      $vendor_data = get_term_meta(absint($v->term_id), 'vendor_data', true);
      $logo = !empty($vendor_data['logo']) ? $vendor_data['logo'] : '';
      $logo_image_url = wp_get_attachment_image_src($logo, 'medium');
      $v->logo = $logo_image_url;
      $v->description = $vendor_data['profile'];
      $v->affiliate_link = get_field('affiliate_link', "wcpv_product_vendors_$v->term_id");
    }
    return $vendors;
  }
  private static function __getInternalData($_p, $similar = false)
  {
    $__p        = $_p->get_data();
    $__p['image_id'] = wp_get_attachment_url($_p->get_image_id());
    $__p['gallery_image_ids'] = array_map(function ($id) {
      return wp_get_attachment_url($id);
    }, $__p['gallery_image_ids']);
    $__p['custom_fields'] = get_fields($__p['id']);
    $__p['vendor'] = AponWCProduct::__getVendorDataByProductId($__p['id']);
    // Price html
    // $__p['price_html'] = $_p->get_price_html();
    // If slug is provided
    if ($similar) {
      $__p['similar_products'] = [];
      $similar_products        = wc_get_related_products($_p->get_id());
      foreach ($similar_products as $value) {
        $_wcSmlp      = wc_get_product($value);
        $_smlp        = $_wcSmlp->get_data();
        $_smlp['image_id'] = wp_get_attachment_url($_wcSmlp->get_image_id());
        $_smlp['custom_fields'] = get_fields($_smlp['id']);
        $_smlp['vendor'] = AponWCProduct::__getVendorDataByProductId($_smlp['id']);
        unset($_smlp['meta_data']);
        array_push($__p['similar_products'], $_smlp);
      }
    }
    unset($__p['meta_data']);
    return $__p;
  }
  public static function listProduct($data)
  {
    $arg = [
      'limit'       => 20,
      // 'paginate'    => true
    ];
    // $page = $data['page'] ?? 1;
    // if (isset($page)) $arg['page'] = (int) $page;
    // If search
    $arg['status'] = $data['status'] ?? 'publish';

    if (isset($data['s'])) {
      $arg['s'] = $data['s'];
    }

    if (isset($data['perPage'])) {
      $arg['limit'] = $data['perPage'];
    }

    // If featured
    if (isset($data['featured']) && $data['featured'] === 'true') {
      $arg['featured'] = true;
    }
    // If category slug is provided
    if (isset($data['category'])) {
      $arg['category'] = array($data['category']);
    }
    // If slug is provided
    if (isset($data['slug'])) {
      // Find product id from slug
      $_product       = get_page_by_path($data['slug'], OBJECT, 'product');
      $arg['include'] = [$_product->ID];
    }

    // If ids is provided
    if (isset($data['include'])) {
      $arg['include'] = $data['include'];
    }
    // 
    if ($data['taxonomies'] && $data['taxonomies'][0]) $taxonomies = is_array($data['taxonomies']) ? $data['taxonomies'] : [$data['taxonomies']];
    if (isset($taxonomies)) {
      $arg['tax_query'] = [
        'relation' => isset($data['taxonomyRelation']) ? $data['taxonomyRelation'] : 'AND'
      ];
      foreach ($taxonomies as $taxonomy) {
        $arg['tax_query'][] = [
          'taxonomy' => $taxonomy[0],
          'field' => $taxonomy[2] ? $taxonomy[2] : 'term_id',
          'terms' => $taxonomy[1]
        ];
      }
    }

    // Query zone, do not mess around
    $_products = wc_get_products($arg);
    $products  = [];
    foreach ($_products as $product) {
      array_push($products, AponWCProduct::__getInternalData($product, count($_products) === 1 ? true : false));
    }
    // return $products;
    $toReturn = [
      'posts'      => $products,
      // 'page'       => (int) $page,
      'totalPages' => (int) ($_products->max_num_pages),
      // 'totalPosts' => $totalPosts,
      'timestamp'  => time(),
      // '_query' => $query->query,
      // '_singular' => $query->is_singular
    ];
    $result = new WP_REST_Response($toReturn, 200);
    $result->set_headers([
      'Pragma' => 'cache',
      'Cache-Control' => 'public, max-age=1800, s-max-age=1800'
    ]);
    return $result;
  }
}

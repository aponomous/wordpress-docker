<?php
// 
add_action('rest_api_init', function () {
  register_rest_route('v1', '/user-point', array(
    'methods'  => 'POST',
    'callback' => function ($x) {
      return _UserPoint_::updateUserPoint($x);
    },
  ));
});

class _UserPoint_
{
  public static function updateUserPoint(WP_REST_Request $request)
  {
    // $secret = isset($request['message']['secret']) ? $request['message']['secret'] : '';
    // if ($secret !== 'B&EpWZt%8Cnk') return new WP_REST_Response(null, 200);
    $body = $request->get_json_params();
    $data = isset($body['message']['data']) ? $body['message']['data'] : null;
    if (!$data) return;
    if (!$data['userId']) return;
    update_field('point', $data, 'user_' . $data['userId']);

    // If correct secret
    return new WP_REST_Response(['success' => true], 200);
  }
}
